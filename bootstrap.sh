#!/usr/bin/env bash

APP_DIR=/webapps
APP_NAME=production

GIT_DIR=/git
GIT_PASSWORD=password

DB_USER=user
DB_PASSWORD=password
DB_NAME=db

SERVER_NAME=example.com


apt-get update
apt-get install -y openjdk-6-jre tomcat7 git postgresql postgresql-contrib --no-install-recommends ubuntu-desktop

# Add required folders
mkdir $GIT_DIR
mkdir -p $APP_DIR/$APP_NAME/src
mkdir $APP_DIR/$APP_NAME/bin

# setup groups & users
echo "Setting up Users & Groups"
#create group
groupadd --system managers
#create users
useradd --system --gid managers --home $GIT_DIR git
echo git:$GIT_PASSWORD | chpasswd
useradd --system --gid managers --home $APP_DIR/$APP_NAME $APP_NAME


# setup posgresSQL
# sudo -u postgres psql -f /vagrant/createdb.sql -v user=$DB_USER -v db=$DB_NAME -v passwd=$DB_PASSWORD


#setup tomcat
